module YTC.Video where

import           Control.Lens
import           Data.Time        (UTCTime)
import           Data.Time.Format (defaultTimeLocale, formatTime)
import           Prelude          hiding (id)

import           YTC.Data

fullYTVideoUrl :: Video -> String
fullYTVideoUrl vid = "https://www.youtube.com/watch?v=" <> vid ^. id

formatVideo :: Video -> String
formatVideo vid = dateStr <> " " <> vid ^. channelTitle <> ": [" <> vid ^. title <> "] " <> url
  where
    dateStr = formatDateFixLen $ vid ^. date
    url = fullYTVideoUrl vid

formatDateFixLen :: UTCTime -> String
formatDateFixLen = formatTime defaultTimeLocale "%_e.%_m. %_k:%M"

formatDateVarLen :: UTCTime -> String
formatDateVarLen = formatTime defaultTimeLocale "%-e.%-m. %-k:%0M"
