{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE OverloadedStrings      #-}
{-# LANGUAGE TemplateHaskell        #-}

module YTC.Data where

import           Control.Lens               (makeFields, (&), (^.))
import           Control.Lens.TH            (makeFields, makeLensesFor)
import           Data.Aeson                 hiding (Options, Result)
import           Data.Aeson.Lens
import           Data.Aeson.TH              hiding (Options)
import           Data.Aeson.Types           (typeMismatch)
import qualified Data.ByteString.Lazy.Char8 as BL
import           Data.Char                  (toLower)
import qualified Data.Text                  as T
import           Data.Time.Clock            (UTCTime)
import           GHC.Exts                   (sortWith)
import           GHC.Generics               (Generic)
import           Graphics.Vty               (Event)
import           Text.Read                  (readMaybe)

import           YTC.UtilsTH

data Channel = Channel
  { _channelId    :: String
  , _channelLabel :: String
  } deriving (Show, Eq, Generic)

makeFields ''Channel

$(deriveJSON defaultOptions {fieldLabelModifier = convertFieldName "_channel"} ''Channel)

data Action
  = Hide
  | SemiHide
  | Highlight
  | NoAction
  deriving (Show, Eq, Generic)

deriveJSON defaultOptions ''Action

data Operation = Operation
  { _operationRegex  :: String
  , _operationAction :: Action
  } deriving (Show, Eq, Generic)

makeFields ''Operation

$(deriveJSON defaultOptions {fieldLabelModifier = convertFieldName "_operation"} ''Operation)

data Interface
  = RawTerminal
  | Terminal
  | TUIList
  deriving (Show, Eq, Generic)

deriveJSON defaultOptions ''Interface

data ControlAction
  = RunCommandControlAction String
  | QuitControlAction
  deriving (Show, Eq, Generic)

deriveJSON defaultOptions ''ControlAction

instance FromJSON Event where
  parseJSON (String x) =
    case (readMaybe $ T.unpack x) :: Maybe Event of
      Just e -> return e
      _      -> fail $ "Failed to parse Event: " <> T.unpack x
  parseJSON x = typeMismatch "String" x

instance ToJSON Event where
  toJSON x = String $ show x & T.pack

data ControlDescription = ControlDescription
  { _controlDescriptionEvent  :: Event
  , _controlDescriptionAction :: ControlAction
  } deriving (Show, Eq, Generic)

makeFields ''ControlDescription

$(deriveJSON defaultOptions {fieldLabelModifier = convertFieldName "_controlDescription"} ''ControlDescription)

data Config = Config
  { _configApiKey         :: String
  , _configChannels       :: [Channel]
  , _configMaxResults     :: Int
  , _configReverse        :: Maybe Bool
  , _configOperations     :: [Operation]
  , _configInterface      :: Interface
  , _configPreviewCommand :: Maybe String
  , _configControls       :: Maybe [ControlDescription]
  } deriving (Show, Eq, Generic)

makeFields ''Config

$(deriveJSON defaultOptions {fieldLabelModifier = convertFieldName "_config"} ''Config)

data Video = Video
  { _videoId           :: String
  , _videoDate         :: UTCTime
  , _videoTitle        :: String
  , _videoThumbnail    :: String
  , _videoChannelTitle :: String
  , _videoAction       :: Maybe Action
  } deriving (Show, Eq, Generic)

makeFields ''Video

data Result = Result
  { _resultVideos :: [Video]
  } deriving (Show, Eq, Generic)

instance Semigroup Result where
  (<>) (Result av) (Result bv) = Result $ av <> bv & sortWith (^. date) & Prelude.reverse

instance Monoid Result where
  mempty = Result []

makeFields ''Result

data YTPageInfo = YTPageInfo
  { yTPageInfoTotalResults   :: Int
  , yTPageInfoResultsPerPage :: Int
  } deriving (Show, Eq, Generic)

$(deriveJSON defaultOptions {fieldLabelModifier = convertFieldName "yTPageInfo"} ''YTPageInfo)

makeFields ''YTPageInfo

data YTItemId = YTItemId
  { yTItemIdKind    :: String
  , yTItemIdVideoId :: String
  } deriving (Show, Eq, Generic)

$(deriveJSON defaultOptions {fieldLabelModifier = convertFieldName "yTItemId"} ''YTItemId)

makeFields ''YTItemId

data YTThumbnail = YTThumbnail
  { yTThumbnailUrl    :: String
  , yTThumbnailWidth  :: Int
  , yTThumbnailHeight :: Int
  } deriving (Show, Eq, Generic)

$(deriveJSON defaultOptions {fieldLabelModifier = convertFieldName "yTThumbnail"} ''YTThumbnail)

makeFields ''YTThumbnail

data YTThumbnails = YTThumbnails
  { yTThumbnailsDefault :: YTThumbnail
  , yTThumbnailsMedium  :: YTThumbnail
  , yTThumbnailsHigh    :: YTThumbnail
  } deriving (Show, Eq, Generic)

$(deriveJSON defaultOptions {fieldLabelModifier = convertFieldName "yTThumbnails"} ''YTThumbnails)

makeLensesFor
  [("yTThumbnailsDefault", "_default"), ("yTThumbnailsMedium", "medium"), ("yTThumbnailsHigh", "high")]
  ''YTThumbnails

data YTSnippet = YTSnippet
  { yTSnippetPublishedAt :: String
  , yTSnippetChannelId   :: String
  , yTSnippetTitle       :: String
  , yTSnippetDescription :: String
  , yTSnippetThumbnails  :: YTThumbnails
  } deriving (Show, Eq, Generic)

$(deriveJSON defaultOptions {fieldLabelModifier = convertFieldName "yTSnippet"} ''YTSnippet)

makeFields ''YTSnippet

data YTItem = YTItem
  { yTItemKind    :: String
  , yTItemEtag    :: String
  , yTItemId      :: YTItemId
  , yTItemSnippet :: YTSnippet
  } deriving (Show, Eq, Generic)

$(deriveJSON defaultOptions {fieldLabelModifier = convertFieldName "yTItem"} ''YTItem)

makeFields ''YTItem

data YTResponse = YTResponse
  { yTResponseKind          :: String
  , yTResponseEtag          :: String
  , yTResponseNextPageToken :: String
  , yTResponseRegionCode    :: String
  , yTResponsePageInfo      :: YTPageInfo
  , yTResponseItems         :: [YTItem]
  } deriving (Show, Eq, Generic)

$(deriveJSON defaultOptions {fieldLabelModifier = convertFieldName "yTResponse"} ''YTResponse)

makeFields ''YTResponse
