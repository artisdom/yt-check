module YTC.Utils where

import           Control.Arrow              ((>>>))
import qualified Data.ByteString            as BS
import qualified Data.ByteString.Char8      as C
import qualified Data.ByteString.Lazy       as BL
import qualified Data.ByteString.Lazy.Char8 as CL
import           Data.Either
import           Data.List
import qualified Data.Text                  as T
import           Data.Text.Encoding         (encodeUtf8)
import qualified Data.Text.Lazy             as TL
import           Data.Text.Lazy.Builder     (toLazyText)
import           HTMLEntities.Decoder       (htmlEncodedText)

combineEithers :: [Either e a] -> Either [e] [a]
combineEithers xs =
  if null es
    then Right as
    else Left es
  where
    es = lefts xs
    as = rights xs

unwrapDoubleEither :: Either e (Either e a) -> Either e a
unwrapDoubleEither (Left e) = Left e
unwrapDoubleEither (Right (Left e)) = Left e
unwrapDoubleEither (Right (Right a)) = Right a

decodeHtmlEntities :: String -> String
decodeHtmlEntities = T.pack >>> htmlEncodedText >>> toLazyText >>> TL.unpack

stringToCL :: String -> CL.ByteString
stringToCL = T.pack >>> encodeUtf8 >>> CL.fromStrict
