{-# OPTIONS_GHC -F -pgmF htfpp #-}
{-# LANGUAGE OverloadedStrings #-}

module ParseTest where

import           Control.Monad.Except (runExceptT)
import           Data.Either
import           Data.Function        ((&))
import           Data.Maybe
import qualified Graphics.Vty         as V
import           Test.Framework

import           SampleData
import           YTC.Config
import           YTC.Data
import           YTC.Lib
import           YTC.UtilsTH
import           YTC.YouTube

test_decapitalizeWord :: IO ()
test_decapitalizeWord = do
  assertEqual "odo" $ decapitalizeWord "odo"
  assertEqual "odo" $ decapitalizeWord "Odo"

test_convertFieldName :: IO ()
test_convertFieldName = do
  assertEqual "x" $ convertFieldName "_y" "_yx"
  assertEqual "x" $ convertFieldName "_y" "_yX"
  assertEqual "fieldName" $ convertFieldName "_YT" "_YTFieldName"
  assertEqual "items" $ convertFieldName "_YTResponse" "_YTResponseItems"

test_parseResponseBody :: IO ()
test_parseResponseBody = do
  res <- runExceptT $ parseResponseBody sampleResponse
  assertBool $ res & isRight

test_parseConfig :: IO ()
test_parseConfig = do
  resMin <- runExceptT $ parseConfig minConfig
  assertEqual (Right $ Config "key" [] 1 Nothing [] Terminal Nothing Nothing) resMin
  resFilled <- runExceptT $ parseConfig filledConfig
  let ctrls = Just [ControlDescription (V.EvKey (V.KChar 'c') []) (RunCommandControlAction "echo $url")]
  assertEqual
    (Right $ Config "key" [Channel "4" "L4"] 1 (Just True) [Operation "TTT" Hide] TUIList (Just "PC") ctrls)
    resFilled

test_readConfig :: IO ()
test_readConfig = do
  conf <- runExceptT $ readConfig "yt-check.sample.json"
  let chans = [Channel "UCd2xKD82vb-LbFEgE6MPsGw" "Exclusively Games", Channel "UCG749Dj4V2fKa143f8sE60Q" "Tim Pool"]
  let ops = [Operation "GTA" SemiHide, Operation "Fortnite" Hide, Operation "Minecraft" Highlight]
  let prevCmd = Just "timg $path --size $width,$height --position $x,$y -0"
  let ctrls =
        Just
          [ ControlDescription (V.EvKey V.KEsc []) QuitControlAction
          , ControlDescription (V.EvKey (V.KChar 'q') []) QuitControlAction
          , ControlDescription
              (V.EvKey (V.KChar 'c') [])
              (RunCommandControlAction "echo '$url' | xclip -in -selection clipboard")
          ]
  assertEqual (Right $ Config "FILL API KEY" chans 5 (Just False) ops TUIList prevCmd ctrls) conf
