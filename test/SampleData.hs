{-# LANGUAGE QuasiQuotes #-}

module SampleData where

import           Text.RawString.QQ

sampleResponse :: String
sampleResponse =
  [r|{
    "kind": "youtube#searchListResponse",
    "etag": "\"XpPGQXPnxQJhLgs6enD_n8JR4Qk/VTo_AtD-g_t4lVmuNBt3NtL5Jo8\"",
    "nextPageToken": "CAEQAA",
    "regionCode": "CZ",
    "pageInfo": {
     "totalResults": 63862,
     "resultsPerPage": 1
    },
    "items": [
     {
      "kind": "youtube#searchResult",
      "etag": "\"XpPGQXPnxQJhLgs6enD_n8JR4Qk/JBg-RlliJ-Zc7AUBsjh1A_zaI10\"",
      "id": {
       "kind": "youtube#video",
       "videoId": "axre2GKI7-Q"
      },
      "snippet": {
       "publishedAt": "2019-03-30T18:00:02.000Z",
       "channelId": "UCH-_hzb2ILSCo9ftVSnrCIQ",
       "title": "THIS IS OUR BEST WORK | Gmod TTT",
       "description": "It's Gmod TTT and I think today we might have some of the best rounds of TTT we've ever played! ▻Yogscast TTT Season 2 T-Shirt: https://yogsca.st/TTT2 ...",
       "thumbnails": {
        "default": {
         "url": "https://i.ytimg.com/vi/axre2GKI7-Q/default.jpg",
         "width": 120,
         "height": 90
        },
        "medium": {
         "url": "https://i.ytimg.com/vi/axre2GKI7-Q/mqdefault.jpg",
         "width": 320,
         "height": 180
        },
        "high": {
         "url": "https://i.ytimg.com/vi/axre2GKI7-Q/hqdefault.jpg",
         "width": 480,
         "height": 360
        }
       },
       "channelTitle": "YOGSCAST Lewis & Simon",
       "liveBroadcastContent": "none"
      }
     }
    ]
   }|]

minConfig = [r|{"apiKey":"key","channels":[],"maxResults":1, "operations": [], "interface": "Terminal"}|]

filledConfig = [r|{
  "apiKey": "key",
  "channels": [{"id":"4","label":"L4"}],
  "maxResults": 1,
  "reverse": true,
  "operations": [ {"regex":"TTT","action":"Hide"} ],
  "interface": "TUIList",
  "previewCommand": "PC",
  "controls": [{
    "event": "EvKey (KChar 'c') []",
    "action": {
      "tag": "RunCommandControlAction",
      "contents": "echo $url"
    }
  }]
}|]
